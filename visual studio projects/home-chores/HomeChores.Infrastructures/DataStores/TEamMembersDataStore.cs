﻿using System.Collections.Generic;
using HomeChores.Domain.TeamMembers;
using HomeChores.Domain.TeamMembers.Interfaces;
using HomeChores.Domain.TeamMembers.Models;

namespace HomeChores.Infrastructures.DataStores
{
    public class TeamMembersDataStore : ITeamMembersDataStore
    {

        public static TeamMembersDataStore Current { get; } = new TeamMembersDataStore();
        public ICollection<TeamMemberDto> Kids { get; set; }
        public TeamMembersDataStore()
        {
            Kids = new List<TeamMemberDto>
            {
                new TeamMemberDto{Name = "Adelyn"},
                new TeamMemberDto{Name = "Jack"},
                new TeamMemberDto{Name = "Raka"},
            };
        }
    }
}
