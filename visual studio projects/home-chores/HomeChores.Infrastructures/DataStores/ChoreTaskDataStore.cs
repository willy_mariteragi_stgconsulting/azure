﻿using System.Collections.Generic;
using HomeChores.Domain.ChoreTasks;
using HomeChores.Domain.ChoreTasks.Interfaces;
using HomeChores.Domain.ChoreTasks.Models;

namespace HomeChores.Infrastructures.DataStores
{
    public class ChoreTaskDataStore : IChoreTaskDataStore
    {

        public ICollection<ChoreTaskDto> ChoreTask { get; set; }
        public ChoreTaskDataStore()
        {
            ChoreTask = new List<ChoreTaskDto>
            {
                //Morning
                new ChoreTaskDto{ Id=1, Name = "Make bed",Description = "Include folding blankets"},
                new ChoreTaskDto{ Id=2, Name = "Eat breakfast"},
                new ChoreTaskDto{ Id=3, Name = "Get dress"},

                //After School
                new ChoreTaskDto{ Id=4, Name = "Wash Hands"},
                new ChoreTaskDto{ Id=5, Name = "Eat Snack"},
                new ChoreTaskDto{ Id=6, Name = "Do homework"},
                new ChoreTaskDto{ Id=7, Name = "Help Raka with Reading"},
                new ChoreTaskDto{ Id=8, Name = "Help Raka with Math"},
                new ChoreTaskDto{ Id=9, Name = "Put dishes away"},
                new ChoreTaskDto{ Id=10, Name = "Clean table"},
                new ChoreTaskDto{ Id=11, Name = "Practice piano"},
                
                //Afternoon Chores
                new ChoreTaskDto{ Id=12, Name = "Switch laundry to dryer"},
                new ChoreTaskDto{ Id=13, Name = "Remove Laundry from the dryer"},
                new ChoreTaskDto{ Id=14, Name = "Fold laundry and put it away"},

                //Dinner Time
                new ChoreTaskDto{ Id=15, Name = "Cook dinner"},
                new ChoreTaskDto{ Id=16, Name = "Clean Dishes"},
                new ChoreTaskDto{ Id=17, Name = "Wipe the table"},

                //Other
                new ChoreTaskDto{ Id=18, Name = "Clean room"},
            };
        }
    }
}
