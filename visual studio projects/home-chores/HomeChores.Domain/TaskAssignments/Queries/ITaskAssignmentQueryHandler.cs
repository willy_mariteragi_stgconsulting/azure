﻿using System.Collections.Generic;
using HomeChores.Domain.TaskAssignments.Models;

namespace HomeChores.Domain.TaskAssignments.Queries
{
    public interface ITaskAssignmentQueryHandler
    {
        IEnumerable<TaskAssignmentModel> Handle(TaskAssignmentGetAllQuery query);
    }
}