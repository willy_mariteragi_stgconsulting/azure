﻿using System;

namespace HomeChores.Domain.TaskAssignments.Commands.Requests
{
    public class TaskAssignmentCreateRequest
    {
        public DateTime AssignmentDate { get; set; }
    }
}