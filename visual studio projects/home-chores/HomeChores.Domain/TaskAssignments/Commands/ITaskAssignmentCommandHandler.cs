﻿using HomeChores.Domain.TaskAssignments.Commands.Requests;
using HomeChores.Domain.TaskAssignments.Models;

namespace HomeChores.Domain.TaskAssignments.Commands
{
    public interface ITaskAssignmentCommandHandler
    {
        TaskAssignmentModel Handle(TaskAssignmentCreateRequest request);
    }
}