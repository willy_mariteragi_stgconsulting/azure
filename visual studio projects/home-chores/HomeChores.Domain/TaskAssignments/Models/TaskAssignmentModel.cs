﻿using System;

namespace HomeChores.Domain.TaskAssignments.Models
{
    public class TaskAssignmentModel
    {
        public DateTime AssignmentDate { get; set; }
    }
}