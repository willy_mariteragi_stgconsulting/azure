﻿namespace HomeChores.Domain.TeamMembers.Models
{
    public class TeamMemberDto
    {
        public string Name { get; set; }
    }

    public class CreateTeamMemberRequest
    {
        public string Name { get; set; }
    }

}
