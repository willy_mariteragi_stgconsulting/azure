﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HomeChores.Domain.TeamMembers.Models;

namespace HomeChores.Domain.TeamMembers.Interfaces
{
    public interface ITeamMemberQueryHandler
    {
        Task<IEnumerable<GetAllTeamMemberQuery>> HandleAsync(GetAllTeamMemberQuery getAllTeamMemberQuery);
    }
}
