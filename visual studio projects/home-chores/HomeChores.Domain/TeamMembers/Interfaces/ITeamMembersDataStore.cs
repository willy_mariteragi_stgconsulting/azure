﻿using System.Collections.Generic;
using HomeChores.Domain.TeamMembers.Models;

namespace HomeChores.Domain.TeamMembers.Interfaces
{
    public interface ITeamMembersDataStore
    {
        ICollection<TeamMemberDto> Kids { get; set; }
    }
}