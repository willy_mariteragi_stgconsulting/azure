﻿using System.Threading.Tasks;
using HomeChores.Domain.TeamMembers.Models;

namespace HomeChores.Domain.TeamMembers.Interfaces
{
    public interface ITeamMemberCommandHandler
    {
        Task<TeamMemberDto> HandleAsync(CreateTeamMemberRequest createTeamMemberRequest);
    }
}