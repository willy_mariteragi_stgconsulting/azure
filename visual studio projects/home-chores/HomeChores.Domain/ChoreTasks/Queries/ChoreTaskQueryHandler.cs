﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomeChores.Domain.ChoreTasks.Interfaces;
using HomeChores.Domain.ChoreTasks.Models;
using Microsoft.Extensions.Logging;

namespace HomeChores.Domain.ChoreTasks.Queries
{
    public class ChoreTaskQueryHandler : IChoreTaskQueryHandler
    {
        private readonly IChoreTaskDataStore _choreTaskDataStore;
        private readonly ILogger<ChoreTaskQueryHandler> _logger;

        public ChoreTaskQueryHandler(ILogger<ChoreTaskQueryHandler> logger, IChoreTaskDataStore choreTaskDataStore)
        {
            _logger = logger;
            _choreTaskDataStore = choreTaskDataStore;
        }

        public IEnumerable<ChoreTaskDto> Handle(ChoreTaskGetAllQuery query)
        {
            return _choreTaskDataStore.ChoreTask;
        }

        public ChoreTaskDto Handle(ChoreTaskGetByIdQuery query)
        {
            try
            {
                return _choreTaskDataStore.ChoreTask.First(c => c.Id == query.Id);
            }
            catch (InvalidOperationException ex)
            {
                _logger.LogError($"ChoreTask Get By Id: {query.Id}", ex);
            }

            return null;
        }
    }
}