﻿namespace HomeChores.Domain.ChoreTasks.Queries
{
    public class ChoreTaskGetAllQuery { }

    public class ChoreTaskGetByIdQuery {
        public int Id { get; set; }
    }

}