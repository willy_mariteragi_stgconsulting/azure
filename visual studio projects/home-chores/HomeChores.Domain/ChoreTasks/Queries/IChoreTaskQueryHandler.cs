﻿using System.Collections.Generic;
using HomeChores.Domain.ChoreTasks.Models;

namespace HomeChores.Domain.ChoreTasks.Queries
{
    public interface IChoreTaskQueryHandler
    {
        IEnumerable<ChoreTaskDto> Handle(ChoreTaskGetAllQuery query);
        ChoreTaskDto Handle(ChoreTaskGetByIdQuery query);
    }
}