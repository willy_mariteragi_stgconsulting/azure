﻿using System;
using System.Linq;
using HomeChores.Domain.ChoreTasks.Interfaces;
using HomeChores.Domain.ChoreTasks.Models;

namespace HomeChores.Domain.ChoreTasks.Commands
{
    public class ChoreTaskCommandHandler : IChoreTaskCommandHandler
    {
        private readonly IChoreTaskDataStore _choreTaskDataStore;

        public ChoreTaskCommandHandler(IChoreTaskDataStore choreTaskDataStore)
        {
            _choreTaskDataStore = choreTaskDataStore;
        }

        public ChoreTaskDto Handle(ChoreTaskCreateRequest request)
        {
            if (request == null)
                throw new NullReferenceException("Cannot be null");
            var maxId = _choreTaskDataStore.ChoreTask.Max(c => c.Id);

            var dto = new ChoreTaskDto
            {
                Id = ++maxId,
                Name = request.Name,
                Description = request.Description
            };

            var current = _choreTaskDataStore;
            current.ChoreTask.Add(dto);

            return dto;
        }

        public void Handle(ChoreTaskRemove choreTaskRemove)
        {
            var taskToRemove = _choreTaskDataStore.ChoreTask.FirstOrDefault(ct => ct.Id == choreTaskRemove.Id);
            _choreTaskDataStore.ChoreTask.Remove(taskToRemove);
        }
    }
}
