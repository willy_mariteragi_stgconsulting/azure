﻿using HomeChores.Domain.ChoreTasks.Models;

namespace HomeChores.Domain.ChoreTasks.Commands
{
    public interface IChoreTaskCommandHandler
    {
        ChoreTaskDto Handle(ChoreTaskCreateRequest request);
        void Handle(ChoreTaskRemove choreTaskRemove);
    }
}