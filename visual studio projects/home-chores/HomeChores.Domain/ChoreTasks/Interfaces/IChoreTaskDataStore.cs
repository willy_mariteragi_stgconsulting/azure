﻿using System.Collections.Generic;
using HomeChores.Domain.ChoreTasks.Models;

namespace HomeChores.Domain.ChoreTasks.Interfaces
{
    public interface IChoreTaskDataStore
    {
        //IChoreTaskDataStore Current { get; }
        ICollection<ChoreTaskDto> ChoreTask { get; set; }
    }
}