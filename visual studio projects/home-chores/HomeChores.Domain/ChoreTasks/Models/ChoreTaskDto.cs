﻿using System.ComponentModel.DataAnnotations;

namespace HomeChores.Domain.ChoreTasks.Models
{
    public class ChoreTaskDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Chore task name cannot be null")]
        public string Name { get; set; }

        public string Description { get; set; }
    }

    public class ChoreTaskCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class ChoreTaskRemove
    {
        public int Id { get; set; }
    }
}