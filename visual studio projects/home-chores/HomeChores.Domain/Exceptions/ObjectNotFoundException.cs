﻿using System;

namespace HomeChores.Domain.Exceptions
{
    public class ObjectNotFoundException : Exception
    {
    }
}