using FluentAssertions;
using System;
using System.Linq;
using System.Threading.Tasks;
using HomeChores.Domain.TeamMembers;
using HomeChores.Domain.TeamMembers.Interfaces;
using HomeChores.Domain.TeamMembers.Models;
using Xunit;

namespace Spec
{
    public class TeamMemberSpec
    {
        private readonly ITeamMemberCommandHandler _commandHandler;
        private readonly ITeamMemberQueryHandler _queryHandler;

        public TeamMemberSpec()
        {
            _commandHandler = new TeamMemberCommandHandler();
            _queryHandler = new TeamMemberQueryHandler();
        }

        [Fact(DisplayName = "Add team a member")]
        public async Task AddNewTeamMemberAsync()
        {
            var request = new CreateTeamMemberRequest
            {
                Name = Guid.NewGuid().ToString()
            };
            var actual = await _commandHandler.HandleAsync(request).ConfigureAwait(false);

            //Assert
            actual.Should().Be(request.Name);
        }

        [Fact(DisplayName = "Get list of team member")]
        public async Task GetAllTeamMembersAsync()
        {
            var actual = await _queryHandler.HandleAsync(new GetAllTeamMemberQuery()).ConfigureAwait(false);

            actual.Count().Should().BeGreaterThan(0);
        }
    }
}