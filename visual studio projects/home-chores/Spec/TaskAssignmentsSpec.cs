﻿using System;
using FluentAssertions;
using HomeChores.Domain.TaskAssignments.Queries;
using System.Linq;
using System.Threading.Tasks;
using HomeChores.Domain.TaskAssignments.Commands;
using HomeChores.Domain.TaskAssignments.Commands.Requests;
using Xunit;

namespace Spec
{
    [Trait("Chore task is assignment to a team member", "")]
    public class TaskAssignmentsSpec
    {
        private ITaskAssignmentQueryHandler _queryHandler;
        private ITaskAssignmentCommandHandler _commandHandler;

        [Fact(DisplayName = "Assignment should have an assignment date")]
        public void Assignment_ShouldHave_AssignedDate()
        {
            var actual = (_queryHandler.Handle(new TaskAssignmentGetAllQuery())).FirstOrDefault();
            //ass
            //actual.AssignmentDate.Should().NotBe((DateTime) null);
        }

        [Fact(DisplayName = "Assignment date should be within 2 weeks")]
        public async Task AssignmentDate_ShouldBe_Within2Weeks()
        {
            var request = new TaskAssignmentCreateRequest{};
            var actual = _commandHandler.Handle(request);

            //Assert
            actual.AssignmentDate.Should().Be(request.AssignmentDate);
        }

        [Fact(DisplayName = "task assignment date cannot be after 2 weeks.")]
        public async Task Assgnment_ShouldThrow_IfAssignmentDateAfter2Weeks()
        {
        }
        [Fact(DisplayName = "Chore task assignment cannot be before today.")]
        public async Task Test3()
        {
        }
    }
}