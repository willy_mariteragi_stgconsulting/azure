﻿using HomeChores.Domain.ChoreTasks;
using HomeChores.Domain.ChoreTasks.Commands;
using HomeChores.Domain.ChoreTasks.Interfaces;
using HomeChores.Domain.ChoreTasks.Queries;
using HomeChores.Infrastructures.DataStores;
using Microsoft.Extensions.Logging;

namespace Spec.Factory
{
    public class SpecFactory
    {
        private static IChoreTaskDataStore _choreTaskDataStore;

        private static IChoreTaskDataStore GetChoreTaskDataStore()
        {
            return _choreTaskDataStore ?? (_choreTaskDataStore = new ChoreTaskDataStore());
        }

        public static IChoreTaskQueryHandler GetChoreTaskQueryHandler()
        {
            return new ChoreTaskQueryHandler(new Logger<ChoreTaskQueryHandler>(new LoggerFactory()), GetChoreTaskDataStore());
        }

        public static IChoreTaskCommandHandler GetChoreTaskCommanHandler()
        {
            return new ChoreTaskCommandHandler(GetChoreTaskDataStore());
        }
    }
}