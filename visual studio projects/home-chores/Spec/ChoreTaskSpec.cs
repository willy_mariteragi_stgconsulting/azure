using FluentAssertions;
using HomeChores.Domain.ChoreTasks.Commands;
using HomeChores.Domain.ChoreTasks.Models;
using HomeChores.Domain.ChoreTasks.Queries;
using System;
using System.Linq;
using Xunit;

namespace Spec
{
    public class ChoreTaskSpec
    {
        private readonly IChoreTaskCommandHandler _commandHandler;
        private readonly IChoreTaskQueryHandler _queryHandler;

        public ChoreTaskSpec()
        {
            _commandHandler = Factory.SpecFactory.GetChoreTaskCommanHandler();
            _queryHandler = Factory.SpecFactory.GetChoreTaskQueryHandler();
        }

        [Fact(DisplayName = "Can add a new Chore Task")]
        public void AddNewChoreTask()
        {
            var request = new ChoreTaskCreateRequest { Name = Guid.NewGuid().ToString(), Description = Guid.NewGuid().ToString() };
            var actual = _commandHandler.Handle(request);

            //Assert
            actual.Should().NotBeNull();
            actual.Name.Should().Be(request.Name);
            actual.Description.Should().Be(request.Description);
        }

        [Fact(DisplayName = "Delete a task")]
        public void DeleteChoreTask()
        {
            var currentTask = _queryHandler.Handle(new ChoreTaskGetAllQuery());
            var expected = currentTask.First();

            _commandHandler.Handle(new ChoreTaskRemove { Id = expected.Id });

            //Act
            var actual = _queryHandler.Handle(new ChoreTaskGetByIdQuery { Id = expected.Id });

            //Assert
            actual.Should().BeNull();
        }

        [Fact(DisplayName = "Get all available chore tasks")]
        public void ChoreTask()
        {
            var actual = _queryHandler.Handle(new ChoreTaskGetAllQuery());

            //Assert
            actual.Should().NotBeNullOrEmpty();
        }

        [Fact(DisplayName = "Get by id ")]
        public void GetById()
        {
            var choreTasks = _queryHandler.Handle(new ChoreTaskGetAllQuery());
            var expected = choreTasks.First();
            var actual = _queryHandler.Handle(new ChoreTaskGetByIdQuery { Id = expected.Id });

            //Assert
            actual.Should().BeEquivalentTo(expected);
        }
    }
}