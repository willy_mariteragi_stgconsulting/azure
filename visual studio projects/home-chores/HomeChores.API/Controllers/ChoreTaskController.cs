﻿using System;
using System.Globalization;
using HomeChores.Domain.ChoreTasks;
using HomeChores.Domain.ChoreTasks.Commands;
using HomeChores.Domain.ChoreTasks.Interfaces;
using HomeChores.Domain.ChoreTasks.Models;
using HomeChores.Domain.ChoreTasks.Queries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeChores.API.Controllers
{
    [Route("api/choretasks")]
    public class ChoreTaskController : Controller
    {
        private readonly ILogger _logger;
        private readonly IChoreTaskCommandHandler _commandHandler;
        private readonly IChoreTaskQueryHandler _queryHandler;

        public ChoreTaskController(ILogger<ChoreTaskController> logger, IChoreTaskCommandHandler commandHandler, IChoreTaskQueryHandler queryHandler)
        {
            _logger = logger;
            _commandHandler = commandHandler;
            _queryHandler = queryHandler;
        }

        [HttpGet]
        public IActionResult GetChoreTasks()
        {
            _logger.LogInformation("Get All Called");
            var models = _queryHandler.Handle(new ChoreTaskGetAllQuery());
            return Ok(models);
        }

        [HttpGet("{id}", Name = "GetChoreTaskById")]
        public IActionResult GetChoreTaskById(int id)
        {
            var dto = _queryHandler.Handle(new ChoreTaskGetByIdQuery { Id = id });
            if (dto == null)
            {
                return NotFound();
            }

            return Ok(dto);
        }

        [HttpPost]
        public IActionResult CreateChoreTask([FromBody] ChoreTaskCreateRequest request)
        {
            var dto = _commandHandler.Handle(request);

            return CreatedAtRoute("GetChoreTaskById", new { id = dto.Id }, dto);

        }
        [HttpPost("error")]
        public IActionResult ThrowAnError()
        {
            try
            {
                throw new CultureNotFoundException("Error");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error End Point Called", ex);
                throw;
            }


        }
    }
}
