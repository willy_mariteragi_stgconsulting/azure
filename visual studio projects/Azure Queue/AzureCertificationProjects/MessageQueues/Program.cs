﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Queue;

namespace MessageQueues
{
    //https://docs.microsoft.com/en-us/azure/storage/queues/storage-tutorial-queues
    class Program
    {
        static readonly string connectionString = "DefaultEndpointsProtocol=https;AccountName=stgwillyazurestorage;AccountKey=NxXoB4Y+FeXwyl6ZUic5FvbQblbV1B+PZjl4muX1T/I4BV/dZjapvBE6SKpIintv7zkG7RmH3JSllGJqfO4hSg==;EndpointSuffix=core.windows.net";
        static readonly string queueName = "willystoragequeue";
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            MainAsync().GetAwaiter().GetResult();
        }
        private static async Task MainAsync()
        {
            // Create a unique name for the queue
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue queue = queueClient.GetQueueReference(queueName);

            // Create the queue
            bool createdQueue = await queue.CreateIfNotExistsAsync().ConfigureAwait(false);

            if (createdQueue)
            {
                Console.WriteLine("The queue was created.");
            }
            Console.WriteLine("======================================================");
            Console.WriteLine("Press ENTER key to continue after sending all the messages.");
            Console.WriteLine("======================================================");
            await SendMessagesAsync(queue, 10).ConfigureAwait(false);
            Console.ReadKey();

            Console.WriteLine("======================================================");
            Console.WriteLine("Press ENTER key to Exit Back after Receiving all the messages.");
            Console.WriteLine("======================================================");

            //Send Messages
            await ReceiveMessageAsync(queue).ConfigureAwait(false);
            Console.ReadKey();

        }
        private static async Task SendMessagesAsync(CloudQueue theQueue, int numberOfMessages)
        {
            try
            {
                for (int i = 0; i < numberOfMessages; i++)
                {
                    string messageBody = $"Message: {i}";
                    var message = new CloudQueueMessage(messageBody);

                    Console.WriteLine($"Sending: {messageBody}");

                    await theQueue.AddMessageAsync(message).ConfigureAwait(false);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {ex.Message}");
                throw;
            }
        }

        static async Task ReceiveMessageAsync(CloudQueue theQueue)
        {

            while (await theQueue.ExistsAsync().ConfigureAwait(false))
            {

                CloudQueueMessage retrievedMessage = await theQueue.GetMessageAsync().ConfigureAwait(false);

                if (retrievedMessage != null)
                {
                    string theMessage = retrievedMessage.AsString;
                    await theQueue.DeleteMessageAsync(retrievedMessage).ConfigureAwait(false);
                    Console.WriteLine($"Message Received: {theMessage}");
                }
            }
        }
    }
}
