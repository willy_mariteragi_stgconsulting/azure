﻿namespace AzureCertificationProjects.Models
{
    public class MessageQueueSendMessageRequest
    {
        public string Message { get; set; }
    }
}
