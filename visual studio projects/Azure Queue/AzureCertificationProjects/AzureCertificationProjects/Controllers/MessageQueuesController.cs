﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AzureCertificationProjects.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.ServiceBus;

namespace AzureCertificationProjects.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageQueuesController : ControllerBase
    {
        const string serviceBusConnectionString = "Endpoint=sb://willytesting.servicebus.windows.net/;SharedAccessKeyName=willy;SharedAccessKey=B0iclKjMouxhjwD2BFrM1hKo4Uk6c3OdNk5rZ2DUYO4=";

        const string QueueName = "willy_queue";
        private readonly IQueueClient queueClient;
        public MessageQueuesController()
        {
            queueClient = new QueueClient(serviceBusConnectionString, QueueName);
        }
        // POST api/values
        [HttpPost]
        public async Task<HttpResponseMessage> PostSendMessage([FromBody] MessageQueueSendMessageRequest value)
        {
            if (value == null)
                return new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest);

            
            var message = new Message(Encoding.UTF8.GetBytes(value.Message));
            await queueClient.SendAsync(message).ConfigureAwait(false);

            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            
        }
    }
}
