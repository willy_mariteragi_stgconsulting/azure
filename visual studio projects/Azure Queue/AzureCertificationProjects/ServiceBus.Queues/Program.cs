﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;

//https://docs.microsoft.com/en-us/azure/service-bus-messaging/service-bus-dotnet-get-started-with-queues
namespace ServiceBus.Queues
{
    class Program
    {
        
        const string serviceBusConnectionString = "Endpoint=sb://willytesting.servicebus.windows.net/;SharedAccessKeyName=willy;SharedAccessKey=B0iclKjMouxhjwD2BFrM1hKo4Uk6c3OdNk5rZ2DUYO4=";

        const string QueueName = "willy_queue";
        static IQueueClient queueClient;

        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            const int numberOfMessages = 10;
            queueClient = new QueueClient(serviceBusConnectionString, QueueName);

            Console.WriteLine("======================================================");
            Console.WriteLine("Press ENTER key to exit after sending all the messages.");
            Console.WriteLine("======================================================");


            //Send Messages
            await SendMessagesAsync(numberOfMessages).ConfigureAwait(false);
            Console.ReadKey();
            await queueClient.CloseAsync().ConfigureAwait(false);
        }

        private static async Task SendMessagesAsync(int numberOfMessages)
        {
            try
            {
                for (int i = 0; i < numberOfMessages; i++)
                {
                    string messageBody = $"Message: {i}";
                    var message = new Message(Encoding.UTF8.GetBytes(messageBody));

                    Console.WriteLine($"Sending: {messageBody}");

                    await queueClient.SendAsync(message).ConfigureAwait(false);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {ex.Message}");
                throw;
            }
        }
    }
}

