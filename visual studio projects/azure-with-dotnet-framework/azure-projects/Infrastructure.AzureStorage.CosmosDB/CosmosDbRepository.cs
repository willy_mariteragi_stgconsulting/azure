﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace Infrastructure.AzureStorage.CosmosDB
{
    public class CosmosDbRepository : ICosmosDbRepository
    {
        public DocumentClient Client { get; }

        public CosmosDbRepository(string endPoint, string masterKey)
        {
            Client = new DocumentClient(new Uri(endPoint), masterKey);
        }

        public async Task<Database> CreateDatabaseIfNotExistsAsync(string databaseName)
        {
            var databaseDefinition = new Database { Id = databaseName };
            var result = await Client.CreateDatabaseIfNotExistsAsync(databaseDefinition).ConfigureAwait(false);
            return result.Resource;
        }

        public void Dispose()
        {
            Client?.Dispose();
        }
    }

    public interface ICosmosDbRepository : IDisposable
    {
        DocumentClient Client { get; }
        Task<Database> CreateDatabaseIfNotExistsAsync(string databaseName);
    }
}
