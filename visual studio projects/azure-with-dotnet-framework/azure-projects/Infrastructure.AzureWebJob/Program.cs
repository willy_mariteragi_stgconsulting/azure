﻿using Microsoft.Azure.WebJobs;

namespace Infrastructure.AzureWebJob
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var host = new JobHost(null,null);
            host.CallAsync(typeof(Functions).GetMethod("Run"));
        }
    }
}
