﻿using Domain.Services;
using Microsoft.Azure.WebJobs;

namespace Infrastructure.AzureWebJob
{
    internal class Functions
    {
        [NoAutomaticTrigger]
        public void ActivateRun()
        {
            IService service = new Service();

            service.Run();
        }
    }
}