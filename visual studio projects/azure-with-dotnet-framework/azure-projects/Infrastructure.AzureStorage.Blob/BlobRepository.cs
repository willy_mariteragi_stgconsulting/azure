﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Infrastructure.AzureStorage.Blob
{

    public class BlobRepository : IBlobRepository
    {
        private readonly CloudBlobContainer _blobContainer;

        public BlobRepository(CloudStorageAccount cloudStorageAccount, string containerName)
        {
            Console.WriteLine("Account Created");
            Console.WriteLine("\tBlobEndpoint: {0}", cloudStorageAccount.BlobEndpoint.AbsoluteUri);
            Console.WriteLine("\tTableEndpoint: {0}", cloudStorageAccount.TableEndpoint.AbsoluteUri);
            Console.WriteLine("\tQueueEndpoint: {0}", cloudStorageAccount.QueueEndpoint.AbsoluteUri);
            Console.WriteLine();

            var blobClient = cloudStorageAccount.CreateCloudBlobClient();
            _blobContainer = blobClient.GetContainerReference(containerName.ToLowerInvariant());

            if (_blobContainer.CreateIfNotExists())
            {
                SetBlobContainerPermissionsToBePublic();
            }
        }

        private void SetBlobContainerPermissionsToBePublic()
        {
            BlobContainerPermissions permissionsGivePublicAccessToBlob = new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            };
            _blobContainer.SetPermissions(permissionsGivePublicAccessToBlob);
        }

        private static string GetBlobUrl(CloudBlockBlob blockBlob)
        {
            return blockBlob.Uri.AbsoluteUri.Replace("https://", "http://");
        }

        public async Task<string> InsertAsync(string blobName, Stream content, string contentType = null)
        {
            var blockBlob = _blobContainer.GetBlockBlobReference(blobName);

            if (!string.IsNullOrWhiteSpace(contentType))
                blockBlob.Properties.ContentType = contentType;

            var accessCondition = AccessCondition.GenerateIfNoneMatchCondition("*");

            await blockBlob.UploadFromStreamAsync(content, accessCondition, null, null).ConfigureAwait(false);

            return GetBlobUrl(blockBlob);

        }


        public async Task<string> InsertAsync(string blobName, byte[] content, string contentType = null)
        {
            var blockBlob = _blobContainer.GetBlockBlobReference(blobName);

            if (!string.IsNullOrWhiteSpace(contentType))
                blockBlob.Properties.ContentType = contentType;

            var accessCondition = AccessCondition.GenerateIfNoneMatchCondition("*");

            await blockBlob.UploadFromByteArrayAsync(content, 0, content.Length, accessCondition, null, null).ConfigureAwait(false);

            return GetBlobUrl(blockBlob);
        }

        public async Task<Stream> GetByNameToStreamAsync(string blobName)
        {
            var blob = _blobContainer.GetBlockBlobReference(blobName);
            var fileStream = new MemoryStream();
            await blob.DownloadToStreamAsync(fileStream).ConfigureAwait(false);

            return fileStream;
        }
        public async Task<byte[]> GetByNameToByArrayAsync(string blobName)
        {
            var blob = _blobContainer.GetBlockBlobReference(blobName);
            await blob.FetchAttributesAsync().ConfigureAwait(false);

            var fileByteLength = blob.Properties.Length;
            byte[] fileContents = new byte[fileByteLength];

            await blob.DownloadToByteArrayAsync(fileContents, 0).ConfigureAwait(false);

            return fileContents;
        }
    }
}
