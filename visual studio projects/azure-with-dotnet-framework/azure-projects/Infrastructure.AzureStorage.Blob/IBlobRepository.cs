﻿using System.IO;
using System.Threading.Tasks;

namespace Infrastructure.AzureStorage.Blob
{
    public interface IBlobRepository
    {
        Task<string> InsertAsync(string blobName, Stream content, string contentType = null);
        Task<string> InsertAsync(string blobName, byte[] content, string contentType = null);



        Task<Stream> GetByNameToStreamAsync(string blobName);
        Task<byte[]> GetByNameToByArrayAsync(string blobName);
    }
}