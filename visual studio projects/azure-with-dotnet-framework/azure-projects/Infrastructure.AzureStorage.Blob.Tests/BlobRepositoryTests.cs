﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Infrastructure.AzureStorage.Common;
using Xunit;

namespace Infrastructure.AzureStorage.Blob.Tests
{
    public class BlobRepositoryTests
    {
        private readonly IBlobRepository _repository;

        private const string ContainerName = "Tests";

        public BlobRepositoryTests()
        {
            _repository = new BlobRepository(AzureTestingHelper.GetCloudStorageAccount(), ContainerName);
        }

        [Fact]
        public async Task AddToBlobFromStream_DownloadToValidate_ItDidGetAddedToBlob()
        {
            var blobName = AzureTestingHelper.FileName.ToLower().Replace(".jpg", $"{Guid.NewGuid()}.jpeg");
            var fileStream = AzureTestingHelper.FileStream;


            await _repository.InsertAsync(blobName, fileStream).ConfigureAwait(false);

            var actual = await _repository.GetByNameToStreamAsync(blobName).ConfigureAwait(false);

            //Assert
            actual.Should().NotBeNull();
            var expectedMd5 = fileStream.GetMd5Hash();
            var actualMd5 = actual.GetMd5Hash();

            expectedMd5.Should().Be(actualMd5);
        }
    }
}