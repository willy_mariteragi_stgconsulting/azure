﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Domain.Aggregates;
using Domain.Models;
using Infrastructure.AzureStorage.Common;

namespace Infrastructure.AzureStorage.Table.AppTests
{
    public class TableRepositoryTests
    {
        private readonly TableRepository<UserMailerLogAggregate, UserMailerLogModel> repository;

        public TableRepositoryTests()
        {
            repository = new TableRepository<UserMailerLogAggregate, UserMailerLogModel>(AzureTestingHelper.GetCloudStorageAccount());
        }

        public async Task InsertTest()
        {
            var aggregate = new UserMailerLogAggregate
            {
                Body = "Body Tests",
                Subject = "Subject Test",
                UserId = Guid.NewGuid()
            };

            var actual = await repository.InsertAsync(aggregate).ConfigureAwait(false);
        }
    }
}
