﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;

namespace Infrastructure.AzureStorage.Table.AppTests.Models
{
    public class StorageTableEntity : TableEntity
    {
        private DateTime createdDate;

        public StorageTableEntity(string id)
        {
            DateTime now = DateTime.UtcNow;
            Id = id;

            RowKey = now.ToString("yyyy_MM_dd_HH_mm_ss_fffffff", DateTimeFormatInfo.InvariantInfo);
            createdDate = now;
        }

        public StorageTableEntity()
        {
        }

        public string Id
        {
            get { return PartitionKey; }
            private set { PartitionKey = value; }
        }

        public string CreatedByUserId { get; set; }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public string Content { get; set; }
    }
}
