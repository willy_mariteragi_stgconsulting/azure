﻿using System.Threading.Tasks;

namespace Infrastructure.AzureStorage.Table.AppTests
{
    public class Program
    {
       private static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                var repository = new TableRepositoryTests();

                await repository.InsertTest().ConfigureAwait(false);
            }).Wait();
        }
    }
}
