﻿namespace Domain
{
    public abstract class Aggregate
    {
        public string Id { get; set; }
        public string ETag { get; set; }
    }
}
