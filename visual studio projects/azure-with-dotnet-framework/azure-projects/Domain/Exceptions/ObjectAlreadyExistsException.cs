﻿using System;

namespace Domain.Exceptions
{
    public class ObjectAlreadyExistsException : Exception
    {
        public ObjectAlreadyExistsException()
            : this("Object Already Exist")
        {
        }

        public ObjectAlreadyExistsException(string message)
            : base(message)

        {
        }

        public ObjectAlreadyExistsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
