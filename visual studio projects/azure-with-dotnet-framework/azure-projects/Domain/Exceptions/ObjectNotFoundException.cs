﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Exceptions
{
    public class ObjectNotFoundException : Exception
    {
        public ObjectNotFoundException()
        : this("Object Not Found")
        {
        }

        public ObjectNotFoundException(string message)
            : base(message)

        {
        }

        public ObjectNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
