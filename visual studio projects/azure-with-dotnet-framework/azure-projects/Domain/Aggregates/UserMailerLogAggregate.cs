﻿using System;

namespace Domain.Aggregates
{
    public class UserMailerLogAggregate : Aggregate
    {
        public UserMailerLogAggregate()
        {
            Id = Guid.NewGuid().ToString();
            CreatedAt = DateTime.Now;
        }
        public Guid UserId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime CreatedAt { get; private set; }
    }
}
