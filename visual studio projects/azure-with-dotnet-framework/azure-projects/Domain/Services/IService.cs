﻿namespace Domain.Services
{
    public interface IService
    {
            void Run();
    }
}
