﻿namespace Domain
{
    public interface IAggregateResponseModel
    {
        string ETag { get; set; }
    }
}