﻿using Microsoft.WindowsAzure;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Management.WebSites;

namespace Infrastructure.AzureDeployment
{
    public class WebSiteDeployment : ISwapDeployment
    {
        public async Task SwapDeploymentAsync(string webSpaceName, string webSiteName, string sourceSlotName, string targetSlotName)
        {
            var credentials = CertificateAuthorizationHelper.GetCredentials();

            var client = new WebSiteManagementClient(credentials);
            await client.WebSites.SwapSlotsAsync(webSpaceName, webSiteName, sourceSlotName, targetSlotName).ConfigureAwait(false);
        }
    }
}
