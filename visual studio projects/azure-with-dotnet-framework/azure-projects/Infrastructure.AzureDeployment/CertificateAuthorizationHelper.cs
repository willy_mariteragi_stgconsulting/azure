﻿using System;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using Microsoft.WindowsAzure;

namespace Infrastructure.AzureDeployment
{
    public static class CertificateAuthorizationHelper
    {
        public static SubscriptionCloudCredentials GetCredentials()
        {
            var certificationKey = ConfigurationManager.AppSettings["cert"];
            var subscriptionId = ConfigurationManager.AppSettings["subscriptionId"];

            return new CertificateCloudCredentials(subscriptionId, new X509Certificate2(Convert.FromBase64String(certificationKey)));

        }
    }
}