﻿using System.Threading.Tasks;

namespace Infrastructure.AzureDeployment
{
    public interface ISwapDeployment
    {
        Task SwapDeploymentAsync(string webSpaceName, string webSiteName, string sourceSlotName, string targetSlotName);
    }
}