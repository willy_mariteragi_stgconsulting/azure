﻿using System.Collections.Generic;
using Domain;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Infrastructure.AzureStorage.Table
{
    internal class AggregateTableEntityAdapter<T> : TableEntityAdapter<T>
        where T : Aggregate, new()
    {
        public AggregateTableEntityAdapter(T aggregate)
            : base(aggregate)
        {
            ETag = aggregate.ETag;
        }
        protected override string BuildPartitionKey()
        {
            return nameof(Aggregate.GetType);
        }

        protected override string BuildRowKey()
        {
            return Aggregate.Id;
        }

        protected override void ReadValues(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
        {
            Aggregate.Id = this.RowKey;
        }

        protected override void WriteValues(IDictionary<string, EntityProperty> properties,
            OperationContext operationContext)
        {
            properties.Remove("");
        }

        protected override void SetETagValue(string eTag)
        {
            this.Aggregate.ETag = eTag;
        }
    }
}