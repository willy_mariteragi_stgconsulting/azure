﻿using System;
using System.Collections.Generic;
using Domain;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Infrastructure.AzureStorage.Table
{
    internal abstract class TableEntityAdapter<T> : ITableEntity where T : Aggregate, new()
    {
        private string _eTag;
        private string _partitionKey;
        private string _rowKey;


        public T Aggregate { get; private set; }

        protected TableEntityAdapter() : this(new T()) { }

        protected TableEntityAdapter(T value)
        {
            Aggregate = value ?? throw new ArgumentNullException(nameof(T), "EntityAdapter cannot be constructed from a null value");
        }

        public string PartitionKey
        {
            get => _partitionKey ?? (_partitionKey = BuildPartitionKey());

            set => _partitionKey = value;
        }

        public string RowKey
        {
            get
            {
                if (_rowKey == null)
                    RowKey = BuildRowKey();
                return _rowKey;
            }

            set => _rowKey = value;
        }

        public DateTimeOffset Timestamp { get; set; }

        public string ETag
        {
            get => _eTag;
            set
            {
                _eTag = value;
                SetETagValue(value);
            }
        }

        public void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
        {
            Aggregate = new T();
            TableEntity.ReadUserObject(Aggregate, properties, operationContext);
            ReadValues(properties, operationContext);
        }

        public IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
        {
            var properties = TableEntity.WriteUserObject(Aggregate, operationContext);
            WriteValues(properties, operationContext);
            return properties;
        }

        protected abstract string BuildPartitionKey();
        protected abstract string BuildRowKey();

        protected virtual void ReadValues(IDictionary<string, EntityProperty> properties, OperationContext operationContext) { }
        protected virtual void WriteValues(IDictionary<string, EntityProperty> properties, OperationContext operationContext) { }
        protected virtual void SetETagValue(string eTag) { }

    }
}