﻿using System;
using System.Threading.Tasks;
using Domain;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;

namespace Infrastructure.AzureStorage.Table
{
    public class TableRepository<TA, TResponseModel>
        where TA : Aggregate, new()
        where TResponseModel : IAggregateResponseModel
    {
        private readonly CloudTable _cloudTable;
        public TableRepository(CloudStorageAccount cloudStorageAccount, string tableName = null)
        {
            if (tableName == null)
                tableName = nameof(TResponseModel).Replace("Model", "");

            var tableClient = cloudStorageAccount.CreateCloudTableClient();
            _cloudTable = tableClient.GetTableReference(tableName);

            _cloudTable.CreateIfNotExists();
        }

        public async Task<TResponseModel> InsertAsync(TA aggregate, DateTime? createdDate = null)
        {

            var tableAggregate = new AggregateTableEntityAdapter<TA>(aggregate);

            var insertOperation = TableOperation.Insert(tableAggregate);
            var result = await _cloudTable.ExecuteAsync(insertOperation).ConfigureAwait(false);

            var model = JsonConvert.DeserializeObject<TResponseModel>(result.Result.ToString());
            return model;
        }
    }
}
