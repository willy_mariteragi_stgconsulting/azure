﻿using Domain;
using Microsoft.WindowsAzure.Storage.Table;

namespace Infrastructure.AzureStorage.Table
{
    public class AzureStorageTableHelper
    {
        public static ITableEntity AggregateToTableEntity<TA>(TA aggregate) where TA : Aggregate, new()
        {
            AggregateTableEntityAdapter<TA> test = new AggregateTableEntityAdapter<TA>(aggregate);

            return test;
        }
    }
}