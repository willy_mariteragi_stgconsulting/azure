﻿using Newtonsoft.Json;

namespace Infrastructure.AzureStorage.CosmosDB.Tests.Models
{
    public class Address
    {
        [JsonProperty(PropertyName = "addressType")]
        public string AddressType { get; set; }

        [JsonProperty(PropertyName = "addressLine1")]
        public string AddressLine1 { get; set; }

        [JsonProperty(PropertyName = "location")]
        public Location Location { get; set; }

        [JsonProperty(PropertyName = "postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty(PropertyName = "countryRegionName")]
        public string CountryRegionName { get; set; }
    }
}