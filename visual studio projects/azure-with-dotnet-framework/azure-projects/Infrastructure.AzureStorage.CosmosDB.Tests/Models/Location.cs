﻿using Newtonsoft.Json;

namespace Infrastructure.AzureStorage.CosmosDB.Tests.Models
{
    public class Location
    {
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "stateProvinceName")]
        public string StateProvinceName { get; set; }
    }
}