﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.AzureStorage.CosmosDB.Tests.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;

namespace Infrastructure.AzureStorage.CosmosDB.Tests
{
    public class CosmosDbRepositoryTests
    {
        private ICosmosDbRepository _repository;
        private const string DatabaseName = "TestingDatabaseToAddAndDelete";


        private const string EndPoint = "https://stgwilly.documents.azure.com:443/";
        private const string MasterKey = "LIWj0txK7pVgxq64ctHFiMTXuQiapdAfdr76IPjZrdnFjIQCyRkWwLgZxszBgdHbBt9RXKZGAIUbn4FnwCOjWg==";

        #region Document Tests

        private const string DocumentCollection = "mystore";
        private readonly FeedOptions _feedOptions = new FeedOptions { EnableCrossPartitionQuery = true };

        private Uri MyStoreCollectionUri => UriFactory.CreateDocumentCollectionUri(_testDatabaseName, DocumentCollection);

        public async Task DocumentDemo_Run()
        {
            _repository = new CosmosDbRepository(EndPoint, MasterKey);
            var client = _repository.Client;

            await CreateDatabase(_testDatabaseName).ConfigureAwait(false);
            await CreateCollections(client, DocumentCollection).ConfigureAwait(false);

            await CreateDocuments(client).ConfigureAwait(false);

            QueryDocumentWithSql(client);
            await QueryDocumentWithPaging(client).ConfigureAwait(false);
            QueryDocumentWithLinq(client);

            await ReplaceDocuments(client).ConfigureAwait(false);

            await DeleteDocuments(client).ConfigureAwait(false);

            Console.WriteLine();
            Console.WriteLine("Press Enter To Close");
            Console.ReadLine();

            _repository.Dispose();
        }

        private async Task CreateDocuments(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> Create Documents <<<");
            Console.WriteLine();

            dynamic document1DefinitionDynamic = new
            {
                name = "New Customer1",
                address = new
                {
                    addressType = "Main Office",
                    addressLine1 = "123 Main Street",
                    location = new
                    {
                        city = "Brooklyn",
                        stateProvinceName = "New York"
                    },
                    postalCode = "12339",
                    countryRegionName = "United States"
                }
            };
            Document document1 = await CreateDocumentAsync(client, document1DefinitionDynamic);

            Console.WriteLine($"Created document {document1.Id} from dynamic object");
            Console.WriteLine();

            var document2DefinitionJson = @"
            {
                ""name"": ""New Customer2"",
                ""address"": 
                {
                    ""addressType"": ""Main Office"",
                    ""addressLine1"":""123 Main Street"",
                    ""location"": {
                        ""city"":""Brooklyn"",
                        ""stateProvinceName"":""New York""
                        },
                    ""postalCode"": ""12339"",
                    ""countryRegionName"": ""United States""
                }
            }";
            var document2Object = JsonConvert.DeserializeObject(document2DefinitionJson);
            var document2 = await CreateDocumentAsync(client, document2Object).ConfigureAwait(false);

            Console.WriteLine($"Created document {document2.Id} from Json object");
            Console.WriteLine();


            var document3DefinitionPoco = new Customer
            {
                Name = "New Customer3",
                Address = new Address
                {
                    AddressType = "Main Office",
                    AddressLine1 = "123 Main Street",
                    Location = new Location
                    {
                        City = "Brooklyn",
                        StateProvinceName = "New York"
                    },
                    PostalCode = "12339",
                    CountryRegionName = "United States"
                }
            };

            var document3 = await CreateDocumentAsync(client, document3DefinitionPoco).ConfigureAwait(false);
            Console.WriteLine($"Created document {document3.Id} from POCO object");
            Console.WriteLine();

        }

        private async Task<Document> CreateDocumentAsync(IDocumentClient client, object documentObject)
        {
            var result = await client.CreateDocumentAsync(MyStoreCollectionUri, documentObject).ConfigureAwait(false);
            var document = result.Resource;

            Console.WriteLine($"Created new document: {document.Id}");
            Console.WriteLine(document);
            return result;
        }

        private void QueryDocumentWithSql(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> Query Documents (SQL)<<<");
            Console.WriteLine();

            Console.WriteLine("Querying for new customer documents (SQL)");
            var sql = "SELECT * FROM c WHERE STARTSWITH(c.name, 'New Customer')=true";

            //Query for dynamic objects
            var documents = client.CreateDocumentQuery(MyStoreCollectionUri, sql, _feedOptions).ToList();
            Console.WriteLine($"Found {documents.Count} new documents");

            foreach (var document in documents)
            {
                Console.WriteLine($"\tId: {document.id}; Name: {document.name};");

                //Dynamic object can be converted into a defined type...
                Customer customer = JsonConvert.DeserializeObject<Customer>(document.ToString());
                Console.WriteLine($"\t City: {customer.Address.Location.City}");
            }
            Console.WriteLine();

            //Or query for defined type; e.g: customer
            var customers = client.CreateDocumentQuery<Customer>(MyStoreCollectionUri, sql, _feedOptions).ToList();
            Console.WriteLine($"Found {customers.Count} new customers");
            foreach (var customer in customers)
            {
                Console.WriteLine($"\tId: {customer.Id}; Name: {customer.Name};");
                Console.WriteLine($"\t City: {customer.Address.Location.City}");
            }
            Console.WriteLine();

            Console.WriteLine("Querying fro all document (SQL)");
            sql = "SELECT * FROM c";
            documents = client.CreateDocumentQuery(MyStoreCollectionUri, sql, _feedOptions).ToList();

            Console.WriteLine($"Found {documents.Count} document");
            foreach (var document in documents)
            {
                Console.WriteLine($"\tId: {document.id}; Name: {document.name};");
            }
            Console.WriteLine();
        }

        private async Task QueryDocumentWithPaging(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> Query Documents (paged results) <<<");
            Console.WriteLine();

            Console.WriteLine("Querying for all documents");
            var sql = "SELECT * FROM c";

            var query = client.CreateDocumentQuery(MyStoreCollectionUri, sql, _feedOptions)
                .AsDocumentQuery();
            while (query.HasMoreResults)
            {
                var documents = await query.ExecuteNextAsync().ConfigureAwait(false);
                foreach (var document in documents)
                {
                    Console.WriteLine($"\tId: {document.id}; Name: {document.name};");
                }
            }
            Console.WriteLine();


        }

        private void QueryDocumentWithLinq(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> Query Documents (LINQ)<<<");
            Console.WriteLine();


            var q = from d in client.CreateDocumentQuery<Customer>(MyStoreCollectionUri, _feedOptions)
                    where d.Address.CountryRegionName == "United Kingdom"
                    select new
                    {
                        d.Id,
                        d.Name,
                        d.Address.Location.City
                    };
            var documents = q.ToList();

            Console.WriteLine($"Found {documents.Count} UK customers");
            foreach (var document in documents)
            {
                var d = (dynamic)document;
                Console.WriteLine($"\tId: {d.Id}; Name: {d.Name}");
            }
            Console.WriteLine();
        }


        private async Task ReplaceDocuments(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> Replace Documents <<<");
            Console.WriteLine();

            ViewDocumentFlag(client, _feedOptions);

            Console.WriteLine("Querying for documents to be updated");
            var sql = "SELECT * FROM c WHERE STARTSWITH(c.name, 'New Customer') = true";
            var documents = client.CreateDocumentQuery(MyStoreCollectionUri, sql, _feedOptions).ToList();
            Console.WriteLine($"Found {documents.Count} documents to be updated");

            foreach (var document in documents)
            {
                document.isNew = true;
                var result = await client.ReplaceDocumentAsync(document._self, documents);
                var updatedDocument = result.Resource;
                Console.WriteLine($"Updated document 'isNew' flag: {updatedDocument.isNew}");
            }
            Console.WriteLine();

            ViewDocumentFlag(client, _feedOptions);
        }

        private void ViewDocumentFlag(IDocumentClient client, FeedOptions options)
        {
            Console.WriteLine("Querying for documents with 'isNew' flag");
            var sql = "SELECT VALUE COUNT(c) FROM c WHERE c.isNew = true";
            var count = client.CreateDocumentQuery(MyStoreCollectionUri, sql, options).AsEnumerable().First();
            Console.WriteLine($"Document with isNew flag: {count}");
            Console.WriteLine();
        }

        private async Task DeleteDocuments(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> Delete Documents <<<");
            Console.WriteLine();

            Console.WriteLine("Querying for document to be deleted");
            var sql = "SELECT c._self,c.address.postalCode FROM c WHERE STARTSWITH(C.name, 'New Customer') = true";
            var documentKeys = client.CreateDocumentQuery(MyStoreCollectionUri, sql, _feedOptions).ToList();

            Console.WriteLine($"Found {documentKeys.Count} documents to be deleted");
            foreach (var documentKey in documentKeys)
            {
                var requestOptions = new RequestOptions { PartitionKey = new PartitionKey(documentKey.postalCode) };
                await client.DeleteDocumentAsync(documentKey._self, requestOptions);
            }

            Console.WriteLine($"Deleted {documentKeys.Count} new customer documents");
            Console.WriteLine();

        }

        #endregion

        #region Collection Tests

        private string _testDatabaseName = "mydb";
        private Uri MyDbDatabaseUri => UriFactory.CreateDatabaseUri(_testDatabaseName);

        public async Task CollectionDemo_Run()
        {
            _testDatabaseName = "mydb";
            _repository = new CosmosDbRepository(EndPoint, MasterKey);
            var client = _repository.Client;

            await CreateDatabase(_testDatabaseName).ConfigureAwait(false);

            ViewCollections(client);

            await CreateCollections(client, "Collection1").ConfigureAwait(false);
            await CreateCollections(client, "Collection2", 2500).ConfigureAwait(false);
            ViewCollections(client);

            await DeleteCollections(client, "Collection1").ConfigureAwait(false);
            await DeleteCollections(client, "Collection2").ConfigureAwait(false);

            Console.WriteLine();
            Console.WriteLine("Press Enter To Close");
            Console.ReadLine();

            _repository.Dispose();
        }

        private void ViewCollections(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine($">>> View Collections in {_testDatabaseName} <<<");

            var collections = client.CreateDocumentCollectionQuery(MyDbDatabaseUri).ToList();
            var i = 0;
            foreach (var collection in collections)
            {
                i++;
                Console.WriteLine();
                Console.WriteLine($"Collection #{i}");
                ViewCollection(collection);
            }

            Console.WriteLine();
            Console.WriteLine($"Total collections in {_testDatabaseName} database: {collections.Count}");
        }

        private static void ViewCollection(Resource collection)
        {
            Console.WriteLine($"\t Collection ID: {collection.Id}");
            Console.WriteLine($"\t   Resource ID: {collection.ResourceId}");
            Console.WriteLine($"\t     Self Link: {collection.SelfLink}");
            Console.WriteLine($"\t         E-Tag: {collection.ETag}");
            Console.WriteLine($"\t     Timestamp: {collection.Timestamp}");
        }

        private async Task CreateCollections(IDocumentClient client, string collectionId, int reservedRUs = 1000, string partitionKey = "/partitionkey")
        {
            Console.WriteLine("");
            Console.WriteLine($">>> Create Collection {collectionId} in {_testDatabaseName}<<<");

            var partitionKeyDefinition = new PartitionKeyDefinition();
            partitionKeyDefinition.Paths.Add(partitionKey);

            var collectionDefinition = new DocumentCollection
            {
                Id = collectionId,
                PartitionKey = partitionKeyDefinition
            };
            var options = new RequestOptions { OfferThroughput = reservedRUs };
            var result = await client.CreateDocumentCollectionIfNotExistsAsync(MyDbDatabaseUri, collectionDefinition, options).ConfigureAwait(false);
            var collection = result.Resource;

            Console.WriteLine("Created new collection");
            ViewCollection(collection);
        }

        private async Task DeleteCollections(IDocumentClient client, string collectionId)
        {
            Console.WriteLine("");
            Console.WriteLine($">>> Delete Collection {collectionId} in {_testDatabaseName} <<<");

            var collectionUri = UriFactory.CreateDocumentCollectionUri(_testDatabaseName, collectionId);
            await client.DeleteDocumentCollectionAsync(collectionUri).ConfigureAwait(false);

            Console.WriteLine($"Deleted Collection {collectionId} from database {_testDatabaseName}");

        }

        #endregion

        #region  Database Tests

        public async Task CosmosDbRepositoryTest_Create_View_DeleteDatabase()
        {
            _repository = new CosmosDbRepository(EndPoint, MasterKey);
            var client = _repository.Client;
            ViewDatabase(client);

            await CreateDatabase(DatabaseName).ConfigureAwait(false);
            ViewDatabase(client);

            await DeleteDatabase(client).ConfigureAwait(false);


            Console.WriteLine();
            Console.WriteLine("Press Enter To Close");
            Console.ReadLine();

            _repository.Dispose();
        }

        private static void ViewDatabase(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> View Databases <<<");

            var databases = client.CreateDatabaseQuery().ToList();
            foreach (var database in databases)
            {
                Console.WriteLine($"Database Id:{database.Id}; Rid: {database.ResourceId}");
            }

            Console.WriteLine("");
            Console.WriteLine($"Total databases: {databases.Count}");
        }
        private async Task CreateDatabase(string databaseName)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> Create Database If Not Exists <<<");

            var database = await _repository.CreateDatabaseIfNotExistsAsync(databaseName).ConfigureAwait(false);

            Console.WriteLine($"Database Id:{database.Id}; Rid: {database.ResourceId}");
        }

        private static async Task DeleteDatabase(IDocumentClient client)
        {
            Console.WriteLine("");
            Console.WriteLine(">>> Delete Databases <<<");
            var databaseUri = UriFactory.CreateDatabaseUri(DatabaseName);

            await client.DeleteDatabaseAsync(databaseUri).ConfigureAwait(false);
        }

        #endregion     
    }
}
