﻿using System.Threading.Tasks;

namespace Infrastructure.AzureStorage.CosmosDB.Tests
{
    public class Program
    {
        private static void Main(string[] args)
        {

            Task.Run(async () =>
            {
                var repositoryTest = new CosmosDbRepositoryTests();
                //await repositoryTest.CosmosDbRepositoryTest_Create_View_DeleteDatabase().ConfigureAwait(false);

                //await repositoryTest.CollectionDemo_Run().ConfigureAwait(false);

                await repositoryTest.DocumentDemo_Run().ConfigureAwait(false);
            }).Wait();
        }
    }
}
