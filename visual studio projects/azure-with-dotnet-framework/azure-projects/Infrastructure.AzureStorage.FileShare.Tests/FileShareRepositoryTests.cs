﻿using System;
using System.IO;
using Xunit;
using System.Threading.Tasks;
using FluentAssertions;
using Infrastructure.AzureStorage.Common;

namespace Infrastructure.AzureStorage.FileShare.Tests
{
    public class FileShareRepositoryTests
    {
        private const string DirectoryName = "TestsFileShare";

        private readonly IFileShareRepository _repository;

        public FileShareRepositoryTests()
        {
            _repository = new FileShareRepository(AzureTestingHelper.GetCloudStorageAccount(), DirectoryName);
        }

        [Fact]
        public async Task AddToFileShareAsync_DownloadToValidate()
        {

            //var fileName = "pokemon-willy123.jpeg";
            var fileName = AzureTestingHelper.FileName.ToLower().Replace(".jpg", $"{Guid.NewGuid()}.jpeg");
            var fileStream = AzureTestingHelper.FileStream;

            await _repository.AddAsync(fileStream, fileName).ConfigureAwait(false);

            //Assert
            await Assert_GetByNameAsync_MD5Equal(fileName, fileStream).ConfigureAwait(false);
        }

        private async Task Assert_GetByNameAsync_MD5Equal(string fileName, Stream fileStream)

        {
            var actual = await _repository.GetByNameAsync(fileName).ConfigureAwait(false);

            //Assert
            actual.Should().NotBeNull();
            var expectedMd5 = fileStream.GetMd5Hash();
            var actualMd5 = actual.GetMd5Hash();

            expectedMd5.Should().Be(actualMd5);
        }
    }
}