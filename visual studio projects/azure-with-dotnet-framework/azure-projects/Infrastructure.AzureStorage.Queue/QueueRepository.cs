﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;

namespace Infrastructure.AzureStorage.Queue
{
    public class QueueRepository
    {
        private readonly CloudQueue _cloudQueue;
        public QueueRepository(CloudStorageAccount cloudStorageAccount, string queueName)
        {
            var cloudQueueClient = cloudStorageAccount.CreateCloudQueueClient();
            _cloudQueue = cloudQueueClient.GetQueueReference(queueName);

            _cloudQueue.CreateIfNotExistsAsync();
        }

    }
}
