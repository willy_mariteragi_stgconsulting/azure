﻿using System;
using System.Net;
using Domain.Exceptions;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;

namespace Infrastructure.AzureStorage.Common
{
    public static class CloudStorageAccountHelper
    {
        public static CloudStorageAccount GetCloudStorageAccount(string accountName, string accountKey, bool useHttps = true)
        {
            var storageCredentials = new StorageCredentials(accountName, accountKey);
            return new CloudStorageAccount(storageCredentials, useHttps);
        }

        public static CloudStorageAccount GetCloudStorageAccount(string connectionString)
        {
            return CloudStorageAccount.Parse(connectionString);
        }

        public static HttpStatusCode? GetStatusCode(this StorageException ex)
        {
            if ((ex.InnerException != null) && ex.InnerException is WebException)
            {
                var webException = (WebException)ex.InnerException;
                if ((webException.Response != null) && webException.Response is HttpWebResponse)
                {
                    var httpWebResponse = (HttpWebResponse)webException.Response;

                    return httpWebResponse.StatusCode;
                }
            }

            if (ex.RequestInformation != null)
                return (HttpStatusCode?)ex.RequestInformation.HttpStatusCode;

            return null;
        }

        public  static StorageException ThrowStorageException(this StorageException ex)
        {
            var statusCode = ex.GetStatusCode();
            if (statusCode != null)
            {
                switch (statusCode)
                {
                    case HttpStatusCode.NotFound:
                        throw new ObjectNotFoundException(null, ex);
                    case HttpStatusCode.Conflict:
                        throw new ObjectAlreadyExistsException(null, ex);
                }
            }

            return ex;
        }
    }
}
