﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using Microsoft.WindowsAzure.Storage;

namespace Infrastructure.AzureStorage.Common
{
    public static class AzureTestingHelper
    {
        private const string AccountName = "stgwillyazurestorage";
        private const string AccountKey = "NxXoB4Y+FeXwyl6ZUic5FvbQblbV1B+PZjl4muX1T/I4BV/dZjapvBE6SKpIintv7zkG7RmH3JSllGJqfO4hSg==";
        private const string ConnectionString = "DefaultEndpointsProtocol=https;AccountName=stgwillyazurestorage;AccountKey=NxXoB4Y+FeXwyl6ZUic5FvbQblbV1B+PZjl4muX1T/I4BV/dZjapvBE6SKpIintv7zkG7RmH3JSllGJqfO4hSg==;EndpointSuffix=core.windows.net";

        private const string ImagePath = "Infrastructure.AzureStorage.Common";

        public static CloudStorageAccount GetCloudStorageAccount()
        {
            var cloudStorageAccount = CloudStorageAccountHelper.GetCloudStorageAccount(AccountName, AccountKey);
            return cloudStorageAccount;
        }

        public static string FileName = "pokemon.jpg";

        public static Stream FileStream
        {
            get
            {
                var fileStream = Assembly.GetExecutingAssembly()
                    .GetManifestResourceStream($"{ImagePath}.Images.{FileName}");
                return fileStream;
            }
        }


        public static byte[] GetByteArray()
        {
            var fileStream = FileStream;

            return ((MemoryStream)fileStream)?.ToArray();
        }

        public static bool CompareMemoryStreams(this MemoryStream ms1, MemoryStream ms2)
        {
            //if (ms1.Length != ms2.Length)
            //    return false;
            ms1.Position = 0;
            ms2.Position = 0;

            var msArray1 = ms1.ToArray();
            var msArray2 = ms2.ToArray();

            return msArray1.SequenceEqual(msArray2);
        }
        public static string GetMd5Hash(this FileInfo fileInfo)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fileInfo.FullName))
                {
                    var hashBytes = md5.ComputeHash(stream);
                    return Convert.ToBase64String(hashBytes);
                }
            }
        }
        public static string GetMd5Hash(this Stream stream)
        {
            using (var md5 = MD5.Create())
            {
                var hashBytes = md5.ComputeHash(stream);
                return Convert.ToBase64String(hashBytes);
            }
        }
    }
}
