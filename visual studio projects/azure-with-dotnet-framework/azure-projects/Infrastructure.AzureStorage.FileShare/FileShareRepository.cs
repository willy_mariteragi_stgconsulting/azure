﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Domain.Exceptions;
using Infrastructure.AzureStorage.Common;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;

namespace Infrastructure.AzureStorage.FileShare
{
    public class FileShareRepository : IFileShareRepository
    {
        private readonly CloudFileShare _cloudFileShare;
        public FileShareRepository(CloudStorageAccount cloudStorageAccount, string fileName)
        {

            var fileClient = cloudStorageAccount.CreateCloudFileClient();
            _cloudFileShare = fileClient.GetShareReference(fileName.ToLowerInvariant());

            if (_cloudFileShare.CreateIfNotExists())
            {
                AddFileSharePermissionsIfNotExists(_cloudFileShare);
            }
        }


        public async Task AddAsync(Stream fileContent, string fileName, string directoryName = null, CancellationToken token = default(CancellationToken))
        {
            try
            {

                AddFileSharePermissionsIfNotExists(_cloudFileShare);

                fileName = fileName.ToLowerInvariant();
                var rootDirectory = _cloudFileShare.GetRootDirectoryReference();

                CloudFileDirectory directory = null;
                if (!string.IsNullOrWhiteSpace(directoryName))
                {
                    directory = rootDirectory.GetDirectoryReference(directoryName);
                    directory.CreateIfNotExists();
                }

                CloudFile cloudFile;

                if (directory != null && await directory.ExistsAsync(token).ConfigureAwait(false))
                    cloudFile = directory.GetFileReference(fileName);
                else
                    cloudFile = rootDirectory.GetFileReference(fileName);

                await cloudFile.UploadFromStreamAsync(fileContent, fileContent.Length, token).ConfigureAwait(false);

            }
            catch (StorageException ex)
            {
                throw ex.ThrowStorageException();
            }
        }

        public async Task<Stream> GetByNameAsync(string fileName, string directoryName = null, CancellationToken token = default(CancellationToken))
        {
            try
            {
                var target = new MemoryStream();
                var rootDirectory = _cloudFileShare.GetRootDirectoryReference();

                CloudFileDirectory directory = null;
                if (!string.IsNullOrWhiteSpace(directoryName))
                {
                    directory = rootDirectory.GetDirectoryReference(directoryName);
                }

                CloudFile file;

                if (directory != null && await directory.ExistsAsync(token).ConfigureAwait(false))
                    file = directory.GetFileReference(fileName);
                else
                    file = rootDirectory.GetFileReference(fileName);


                if (!await file.ExistsAsync(token).ConfigureAwait(false))
                    throw new ObjectNotFoundException($"{fileName} does not exist");

                await file.DownloadToStreamAsync(target, token).ConfigureAwait(false);
                return target;
            }
            catch (StorageException ex)
            {
                throw ex.ThrowStorageException();
            }
        }



        private void AddFileSharePermissionsIfNotExists(CloudFileShare share)
        {
            const string policyName = "SharePolicyAllRights";
            var currentTime = DateTime.UtcNow.AddDays(5);
            var datetime = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, 0, 0, 0);

            FileSharePermissions permissions = share.GetPermissions();
            if (permissions.SharedAccessPolicies.ContainsKey(policyName))
            {
                foreach (var permissionsSharedAccessPolicy in permissions.SharedAccessPolicies)
                {
                    if (permissionsSharedAccessPolicy.Key != policyName) continue;

                    if (permissionsSharedAccessPolicy.Value.SharedAccessExpiryTime != datetime)
                    {
                        permissions.SharedAccessPolicies.Remove(permissionsSharedAccessPolicy);
                        break;
                    }

                    return;
                }
            }

            var sharedAccessFilePolicy = new SharedAccessFilePolicy
            {
                Permissions = SharedAccessFilePermissions.Read
                              | SharedAccessFilePermissions.Create
                              | SharedAccessFilePermissions.Write
                              | SharedAccessFilePermissions.Delete
                              | SharedAccessFilePermissions.List,
                SharedAccessExpiryTime = datetime
            };

            permissions.SharedAccessPolicies.Add(policyName, sharedAccessFilePolicy);
            _cloudFileShare.SetPermissions(permissions);
        }

    }
}