﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.AzureStorage.FileShare
{
    public interface IFileShareRepository
    {
        Task AddAsync(Stream fileContent, string fileName, string directoryName = null, CancellationToken token = default(CancellationToken));
        Task<Stream> GetByNameAsync(string fileName, string directoryName = null, CancellationToken token = default(CancellationToken));
    }
}
