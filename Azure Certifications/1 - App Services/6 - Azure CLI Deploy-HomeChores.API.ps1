﻿#Step 1: install Azure CLI
#  - https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest
#Step 2:  login to azzureA
az login

Write-host "#Step 3: Setup variables"
$deploy_gitrepo="https://bitbucket.org/willymariteragi/home-chores/"
$deploy_webappname="HomeChores-API"
$deploy_myResourceGroup="stg-willy-appservice-resourcegroup2"
$deploy_planName="stg-wiilly-hostplan1"

Get-Variable


Write-host "#Step 4: Create Resource Group if not already created"
#az group create --location centralus -n $deploy_myResourceGroup
#Check that the Resource Group is already created 
az group show --name $deploy_myResourceGroup


write-host "#Step 5:  Create an App Service plan in FREE tier. if not already created"
#az appservice plan create --name $deploy_planName --resource-group $deploy_myResourceGroup --sku FREE
az appservice plan show --name $deploy_planName --resource-group $deploy_myResourceGroup


Write-Host "#Step 6: Create the web app first. This will be a blank webApp until you deploy to that webapp"
az webapp create -n $deploy_webappname -g $deploy_myResourceGroup --plan $deploy_planName
    
    write-host "#Step 6.1 Show default hostname"
    az webapp show -n $deploy_webappname -g $deploy_myResourceGroup --query "defaulthostname"



#Step 7: Do the following for continous integration https://docs.microsoft.com/en-us/azure/app-service/deploy-continuous-deployment
    # 7.1: Goto Azure portal
    # 7.2: Goto the App services that you just created above on step 6
    # 7.3: Click on Deployment\Develop Center
    # 7.4: Click on Bitbucket if this is the one you are using as your url repository

#In case that you delete the repository on bitbucket and put it back, you will need to disconnect the deployment from the deployment center and re-add it back for the authorization to reconnect 




#deploy from local git
#az webapp deployment source config-local-git -g $deploy_myResourceGroup -n $deploy_webappname



write-host "#Step 8: Deploy based on the branch"
az webapp deployment source config -n $deploy_webappname -g $deploy_myResourceGroup --repo-url $deploy_gitrepo --branch develop --manual-integration 


Write-host "#Step 9: Sync Changes"
az webapp deployment source sync  -n $deploy_webappname -g $deploy_myResourceGroup

#======================= Browser ===============================
#===============================================================
#open Chrome to ChoreTasks
#Start-Process "chrome" "http://$deploy_webappname.azurewebsites.net/api/choretasks"
Start-Process "chrome" "http://$deploy_webappname.azurewebsites.net"

#KUDU
Start-Process "chrome" "http://$deploy_webappname.scm.azurewebsites.net"


#======================= Browser ===============================
#===============================================================

#Find all outbound IP Addresses
az webapp show -n $deploy_webappname -g $deploy_myresourcegroup --query outboundipaddresses --output tsv
#52.176.6.37,52.165.224.62,52.176.2.238,52.176.0.109,52.165.226.171

az webapp show -n $deploy_webappname -g $deploy_myresourcegroup --query possibleOutboundIpAddresses --output tsv
#52.176.6.37,52.165.224.62,52.176.2.238,52.176.0.109,52.165.226.171,52.165.227.138,52.173.251.170

#show (live) log or Diagnostic Log
az webapp log show -n $deploy_webappname -g $deploy_myResourceGroup 
#turned on failedRequestsTracing and detailedErrorMessages to true
az webapp log config -n $deploy_webappname -g $deploy_myResourceGroup --detailed-error-message true --failed-request-tracing true






#-----------------------------------------------------------------
#--
#-----------------------------------------------------------------
# Different usful command
az account show

az appservice plan create -h
az appservice plan list 

az webapp auth show --resrouce-group $deploy_myResourceGroup

